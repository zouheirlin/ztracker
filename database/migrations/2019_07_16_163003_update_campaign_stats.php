<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCampaignStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_stats', function (Blueprint $table) {
            $table->longText('countries')->nullable();
            $table->longText('isps')->nullable();
            $table->longText('os')->nullable();
            $table->longText('openers')->nullable();
            $table->longText('clickers')->nullable();
            $table->longText('leaders')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_stats', function (Blueprint $table) {
        });
    }
}
