const axios = require('axios');

const actions = {

	updateSidebarWidth({ commit }, width) {
		commit('UPDATE_SIDEBAR_WIDTH', width);
	},
	updateI18nLocale({ commit }, locale) {
		commit('UPDATE_I18N_LOCALE', locale);
	},
	toggleContentOverlay({ commit }) {
		commit('TOGGLE_CONTENT_OVERLAY');
	},
	updateTheme({ commit }, val) {
		commit('UPDATE_THEME', val);
	},

	uploadFile({ commit }, file){
		commit('UPDATE_PERCENTAGE', 0);
		var formData = new FormData();
		formData.append("file", file.file);
		formData.append("description", file.description);
		console.log(formData);
		axios.post('/api/files/add', formData, 
			{	
				headers: {'Content-Type': 'multipart/form-data'},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) );
					commit('UPDATE_PERCENTAGE', uploadPercentage);
				}.bind(this)
			}
		).then(function(res){
			commit('UPDATE_PERCENTAGE', null);
			commit('ADD_FILE', res.data.file);
		})
		.catch(function(){
			commit('UPDATE_PERCENTAGE', null);
		});
	},
	addCampaign({commit}, campaign){
		new Promise((resolve, err) => {
			axios.post('/api/campaigns/add', {campaign: campaign} )
			.then((res) => {
				commit('ADD_CAMPAIGN', res.data.campaign);
				resolve();
			});
		});
		
	},
	addOffer({commit}, offer){
		axios.post('/api/offers/add', {offer: offer} )
		.then((res) => {
			commit('ADD_OFFER', res.data.offer);
		});
	},
	fetchCampaigns({commit}){
		return new Promise((resolve,reject) => {
			axios.get('/api/campaigns')
			.then((res) => {
				commit('UPDATE_CAMPAIGNS', res.data.campaigns);
				resolve(res);
			})
		})
	},
	fetchOffers({commit}){
		return new Promise((resolve,reject) => {
			axios.get('/api/offers')
			.then((res) => {
				commit('UPDATE_OFFERS', res.data.offers);
				resolve(res);
			})
		})
	},
	fetchFiles({commit}){
		return new Promise((resolve,reject) => {
			axios.get('/api/files')
			.then((res) => {
				commit('UPDATE_FILES', res.data.files);
				resolve(res);
			})
		})
	},
	fetchVisitors({commit}, params){
		axios.get(`/api/visitors/${params.view}/${params.id}`)
		.then((res) => {
			commit('UPDATE_VISITORS', res.data.visitors);
		});
	},
	fetchLeads({commit}, id){
		return new Promise((resolve,reject) => {
			axios.get(`/api/leads/${id}`)
			.then((res) => {
				commit('UPDATE_LEADS', res.data.leads);
				resolve(res);
			})
		})
	},
	fetchSettings({commit}){
		return new Promise((resolve,reject) => {
			axios.get('/api/settings')
			.then((res) => {
				commit('UPDATE_SETTINGS', res.data);
				resolve(res);
			})
		})
	},
	saveToken({commit}, token){
		axios.post('/api/settings/token', {token: token})
	},
	login(context){
		context.commit("login");
	},
	deleteCampaign({commit}, id){
		axios.get(`/api/campaigns/delete/${id}`)
		.then((res) => {
			commit('DELETE_CAMPAIGN', id);
		})
	},
	deleteFile({commit}, id){
		axios.get(`/api/files/delete/${id}`)
		.then((res) => {
			commit('DELETE_FILE', id);
		})
	},
	exportOpens({commit}, id){
		commit('UPDATE_EXPPERCENTAGE', 1)
		axios.get(`/api/campaigns/export/opens/${id}`)
		.then((res) => {
			commit('UPDATE_EXPPERCENTAGE', 0)
			commit('EXPORT_CSV', res.data)
		})
	},
	exportClicks({commit}, id){
		commit('UPDATE_EXPPERCENTAGE', 1)
		axios.get(`/api/campaigns/export/clicks/${id}`)
		.then((res) => {
			commit('UPDATE_EXPPERCENTAGE', 0)
			commit('EXPORT_CSV', res.data)
		})
	},
	exportVisitors({commit}, id){
		commit('UPDATE_EXPPERCENTAGE', 1)
		axios.get(`/api/files/export/${id}`)
		.then((res) => {
			commit('UPDATE_EXPPERCENTAGE', 0)
			commit('EXPORT_CSV', res.data)
		})
	},
	exportNoOpen({commit}, id){
		commit('UPDATE_EXPPERCENTAGE', 1)
		axios.get(`/api/campaigns/export/noopens/${id}`)
		.then((res) => {
			commit('UPDATE_EXPPERCENTAGE', 0)
			commit('EXPORT_CSV', res.data)
		})
	},
	refreshFile({commit}, id){
		new Promise((resolve,reject) => {
			axios.get(`/api/files/emails/${id}`)
			.then((res) => {
				commit('UPDATE_FILE_EMAILS', res.data)
				resolve();
			})
		})
	},
	addCampaignFile({commit}, data){
		new Promise((resolve,reject) => {
			axios.post('/api/campaigns/files/add', data)
			.then((res) => {
				//commit('UPDATE_CAMPAIGN', res.data)
				resolve(res.data.message);
			})
		})
	}
}

export default actions