const getters = {
  uploadPercent: state => state.uploadPercent,
  exportPercent: state => state.exportPercent,
  campaigns: state => state.campaigns,
  offers: state => state.offers,
  files: state => state.files,
  visitors: state => state.visitors,
  leads: state => state.leads,
  isLoggedIn: state => state.isLoggedIn,
  currentUser: state => state.currentUser,
  authError: state => state.auth_error,
  token: state => state.settings.token,
}

export default getters