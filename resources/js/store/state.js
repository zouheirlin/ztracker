let colors = {
	primary: '#7367F0',
	success: '#28C76F',
	danger: '#EA5455',
	warning: '#FF9F43',
	dark: '#1E1E1E',
}
import {getLocalUser} from '../auth';
const user = getLocalUser();

const state = {
	isSidebarActive: true,
	breakpoint: null,
	sidebarWidth: "default",
	reduceButton: false,
	bodyOverlay: false,
	sidebarItemsMin: false,
	theme: 'light',
	AppActiveUser: {
		id: 0,
		name: 'John Doe',
		about: 'Dessert chocolate cake lemon drops jujubes. Biscuit cupcake ice cream bear claw brownie brownie marshmallow.',
		img: 'avatar-s-11.png',
		status: 'online',
	},
	themePrimaryColor: colors.primary,
	uploadPercent: null,
	exportPercent: null,
	campaigns: [],
	offers: [],
	files: [],
	leads: [],
	visitors: [],
	currentUser: user,
	isLoggedIn: !!user,
	loading: false,
	auth_error: null,
	settings: {
		token: null,
	}
}

export default state