
export default [
	{
		url: "/campaigns",
		name: "Campaigns",
		slug: "campaigns",
		icon: "LayersIcon",

	},
	{
		url: "/offers",
		name: "Offers",
		slug: "offers",
		icon: "DollarSignIcon",

	},
	{
		url: "/files",
		name: "Files",
		slug: "files",
		icon: "FileTextIcon",
	},
	{
		url: "/settings",
		name: "Settings",
		slug: "settings",
		icon: "SettingsIcon"
	}
]