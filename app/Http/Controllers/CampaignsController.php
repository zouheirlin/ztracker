<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Campaign;
use App\CampaignStat;
use App\File;
use App\Lead;
use App\User;
use App\Visitor;
use App\Offer;
use App\Action;
use App\Jobs\ProcessActions;
use App\Jobs\ProcessStats;

class CampaignsController extends Controller
{
    function __construct(){
        // $this->middleware('auth');
    }

    //------------------------------------------------
    //                      GETTERS
    //------------------------------------------------

    function getCampaigns(){
        $campaigns = Campaign::with('offer')->with('stats')->get();
        return response()->json(['campaigns' => $campaigns]);
    }

    function getLeads($id){
        $leads = Action::whereRaw('campaign_id = '.$id.' and lead = 1')->with('visitor')->with('campaign')->with('campaign.offer')->get();
        return response()->json(['leads' => $leads]);
    }

    function getOffers(){
        $offers = Offer::all();
        return response()->json(['offers' => $offers]);
    }

    function getVisitors($view,$id){
        if($view == "files"){
            $file = File::find($id);
            $visitors = $file->visitors;
            return response()->json(['visitors' => $visitors]);
        }else{
            $campaign = Campaign::find($id);
            $visitors = [];
            return response()->json(['visitors' => $visitors]);
        }
    }

    function exportOpens($id){

        $openers = \DB::table('actions')
            ->join('visitors', 'visitors.id', '=', 'actions.visitor_id')
            ->whereRaw('actions.campaign_id = '.$id.' and actions.open = 1')
            ->select('visitors.email','visitors.hash_md5')
            ->get();
        return ['type' => 'openers', 'result' => $openers];;
    }

    function exportNoOpens($id){   
        $ids = Campaign::find($id)->getFileIdsAttribute();
        $noopeners = Visitor::whereIn('file_id',$ids)
            ->whereNotIn('visitors.id', 
                function($q) use ($id){
                    $q->select('visitor_id')->from('actions')->where('campaign_id','=', $id);
                }
            )
            ->select('visitors.email','visitors.hash_md5')
            ->get();
        
        return ['type' => 'noopeners', 'result' => $noopeners];;
    }

    function exportClicks($id){
        $clickers = \DB::table('actions')
            ->join('visitors', 'visitors.id', '=', 'actions.visitor_id')
            ->whereRaw('actions.campaign_id = '.$id.' and actions.click = 1')
            ->select('visitors.email','visitors.hash_md5')
            ->get();
        
        return ['type' => 'clicks', 'result' => $clickers];
    }

    
    //------------------------------------------------
    //                      SETTERS
    //------------------------------------------------

    function addCampaign(Request $request){
        $campaign = new Campaign();
        $campaign->name = $request->campaign['name'];
        $campaign->offer_id = $request->campaign['offer'];
        $campaign->save();
        $visitors = 0;
        foreach($request->campaign['files'] as $file){
            $fl = File::find($file['id']);
            $visitors += sizeof($fl->visitors);
            $campaign->files()->attach($file['id']);
        }

        $campaignstats = new CampaignStat();
        $campaignstats->campaign_id = $campaign->id;
        $campaignstats->emails = $visitors;
        $campaignstats->save();
        
        $campaign->stats = $campaignstats;
        return response()->json(['campaign' => $campaign]);
    }

    function addOffer(Request $request){
        $offer = new Offer();
        $offer->name = $request->offer['name'];
        $offer->type = $request->offer['type'];
        $offer->sponsor = $request->offer['sponsor'];
        $offer->save();
        return response()->json(['offer' => $offer]);
    }

    function addFile(Request $request){
        $campaign = Campaign::find($request->campaign);
        if(!$campaign->files->contains($request->file['id'])){
            $campaign->files()->attach($request->file['id']);
            $fl = File::find($request->file['id']);
            $visitors = sizeof($fl->visitors);
            $campaign->stats->emails += $visitors;
            $campaign->stats->save(); 
            return response()->json(['message' => 'File added successfully', 'campaign' => $campaign->id, 'file' => $request->file['id']]);
        }
        return response()->json(['message' => 'File already exist to this campaign']);
    }

    function deleteCampaign($id){
        $campaign = Campaign::find($id);
        $campaign->actions()->delete();
        $campaign->stats()->delete();
        $campaign->leads()->delete();
        $campaign->files()->detach();
        //for performance get actions then 
        // Action::whereIn('id', $actions->pluck('id'))->delete(); 
        $campaign->delete();
        return '1';
    }

    //------------------------------------------------
    //                      EVENTS
    //------------------------------------------------

    function saveLead(Request $request){
        $camp = $request->campaign;
        $hash = $request->email;
        //payout
        ProcessActions::dispatch('lead',$camp,$hash,$request->payout);
    }

    function saveClick(Request $request){
        $camp = $request->campaign;
        $hash = $request->email;
        $server = $request->data;
        ProcessActions::dispatch('click',$camp,$hash,$server);
    }

    function saveOpen(Request $request){
        $camp = $request->campaign;
        $hash = $request->email;
        $server = $request->data;
        ProcessActions::dispatch('open',$camp,$hash,$server);
    }

    //------------------------------------------------
    //                      STATISTICS
    //------------------------------------------------


    function setCountries(){
        ProcessStats::dispatch('countries');
    }

    function setIsps(){
        ProcessStats::dispatch('isps');
    }

    function setOs(){
        ProcessStats::dispatch('os');
    }

    function setOpeners(){
        ProcessStats::dispatch('openers');
    }

    function setClickers(){
        ProcessStats::dispatch('clickers');
    }

    function setLeaders(){
        ProcessStats::dispatch('leaders');
    }

    function getStats($id){
        $stats = Campaign::find($id)->stats;
        $countries = json_decode($stats->countries);
        $visitors = $stats->emails;
        $isp = json_decode($stats->isps);
        $os = json_decode($stats->os);
        $openers = json_decode($stats->openers);
        $clickers = json_decode($stats->clickers);
        $leads = json_decode($stats->leaders);
        $load = ['countries' => $countries, 'isp' => $isp, 'os' => $os, 'clickers' => $clickers, 'openers' => $openers, 'leads' => $leads, 'visitors' => $visitors];
        return $load;
    }

    public function create(){
       /* $user = new User();
        $user->name = "user";
        $user->email = "admin@test.com";
        $user->password = Hash::make('123456');
        $user->save();*/
    }
}
