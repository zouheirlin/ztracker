<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Jobs\ProcessFiles;
use Carbon\Carbon;
use App\Visitor;
use App\User;
use App\File;
use App\Action;
use Date;

class EmailsController extends Controller
{
    function __construct(){
       // $this->middleware('auth');
    }

    function uploadFile(Request $request){
        if($request->file){
            
            $name = pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME) . '_' . str_replace(["-"," ",":"], "_", Carbon::now()->toDateTimeString());
            $path = Storage::putFileAs('/files', $request->file('file'), "$name.txt");
            $file = new File();
            $file->name = $name;
            $file->state = 0;
            $file->emails = 0;
            $file->description = $request->description;
            $file->save();
            ProcessFiles::dispatch($path,$file->id);
            //$this->handleFile($path,$file);
            return response()->json(['file' => $file]);
        }
    }

    function filesInt(){
        return view('file');
    }

    function handleFile($path,$file){
        $handle = fopen("storage/".$path, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                echo $buffer . '\n';
                if (strlen($buffer) > 5){
                    if(strpos($buffer, ';') !== false){
                        $line = explode(";", $buffer);
                        if($line[0] != ""){
                            $line[0] = strtolower(rtrim($line[0]));
                            if($line[1] == "")
                                $line[1] = "unknown";
                            $visitor = new Visitor();
                            $visitor->email = $line[0];
                            $visitor->hash_md5 = md5($line[0]);
                            $visitor->full_name = $line[1];
                            $visitor->file_id = $file->id;
                            $visitor->save();
                        }
                    }else{
                        $visitor = new Visitor();
                        $visitor->email = strtolower(rtrim($buffer));
                        $visitor->hash_md5 = md5(rtrim($buffer));
                        $visitor->full_name = "unknown";
                        $visitor->file_id = $file->id;
                        $visitor->save();
                    }
                    
                }
            }
        }
        fclose($handle);
        $count = sizeof($file->visitors);
        $file->state = 1;
        $file->emails = $count;
        $file->save();
    }

    function getFiles(){
        $files = File::all();
        return response()->json(['files' => $files]);
    }

    function deleteFile($id){
        $file = File::find($id);
        $file->visitors()->delete();
        $file->campaigns()->detach();
        $file->delete();
    }

    function exportVisitors($id){   
        $file = File::find($id);
        $visitors = $file->exp_visitors;
        return ['type' => 'visitors', 'result' => $visitors];
    }

    function getFileEmails($id){
        $file = File::find($id);
        $visitors = Visitor::where('file_id', '=', $id)->get()->count();
        if($visitors == $file->emails){
            $file->state = 1;
            $file->save();
        }else{
            $file->emails = $visitors;
            $file->save();
        }
        
        return ['file' => $id, 'emails' => $visitors];
    }
}
